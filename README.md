# SKD_Integration

This project contains the sources and additional results for the paper submitted to PPSN 2024.

## Sources
The source code is available in "source_code.zip". 
To run it, execute "python3 manage_jobs.py".
The test instances and the tested algorithms can be changed in the file "manage_jobs.py".
The parameters of the different algorithms are located in "generate_jobs_cluster.sh".
The creation strategy of groups is defined in "experiments_vrptw.py" for MOEA/D and "experiments_mols.py" for IMOLS.
The resources folder contains the instances, the reference fronts, and the initial fronts.

## Additional Data
All the resulting data is available in "data_ppsn2024.zip".
These results have been aggregated in two data frames, one for the Homberger benchmark, the other one for the Solomon benchmark.
Detailed tables are available in "Additional_Results_PPSN_2024.pdf". 
